"""siteemploi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from emploi.views import IndexView
from emploi.views import LoginView
from emploi.views import LogoutView
from emploi.views import OffresListView
from emploi.views import DepotOffreView
from emploi.views import DetailOffreView
from emploi.views import MyOffresView
from emploi.views import UpdateOffreView
from emploi.views import DeleteOffreView

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', IndexView.as_view(), name="accueil"),
    url(r'^login/', LoginView.as_view(), name="connexion"),
    url(r'^logout/', LogoutView.as_view(), name="deconnexion"),
    url(r'^list/', OffresListView.as_view(), name="list-offres"),
    url(r'^deposer/', DepotOffreView.as_view(), name="depot-offre"),
    url(r'^detail/(?P<pk>\d+)/$', DetailOffreView.as_view(), name="detail-offre"),
    url(r'^update/(?P<pk>\d+)/$', UpdateOffreView.as_view(), name="update-offre"),
    url(r'^delete/(?P<pk>\d+)/$', DeleteOffreView.as_view(), name="delete-offre"),
    url(r'^myoffres', MyOffresView.as_view(), name="my-offres"),
]
