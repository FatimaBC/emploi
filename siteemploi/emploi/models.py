from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Offre (models.Model) :
    intitule = models.CharField(
        max_length=60,
        verbose_name="Intitulé du poste"
    )
    date_poste = models.DateField(
        verbose_name="Date de validité du poste"
    )
    description=models.TextField(
        verbose_name="Description du poste"
    )
    duree=models.CharField(
        verbose_name="Durée du contrat",
        max_length=60,
    )
    lieu=models.CharField(
        verbose_name="Lieu",
        max_length=60,
    )
    competences=models.TextField(
        verbose_name="Compétences requises"
    )
    contact=models.EmailField(
        verbose_name="Pour postuler",
        default="admin@emploi.fr"
    )
    auteur_id=models.ForeignKey(
        User,
        verbose_name="Auteur de l'offre",
        on_delete=models.CASCADE,
    )