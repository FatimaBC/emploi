# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emploi', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='offre',
            old_name='auteur',
            new_name='auteur_id',
        ),
    ]
