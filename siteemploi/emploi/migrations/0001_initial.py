# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Offre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('intitule', models.CharField(verbose_name='Intitulé du poste', max_length=60)),
                ('date_poste', models.DateField(verbose_name='Date de validité du poste')),
                ('description', models.TextField(verbose_name='Description du poste')),
                ('duree', models.CharField(verbose_name='Durée du contrat', max_length=60)),
                ('lieu', models.CharField(verbose_name='Lieu', max_length=60)),
                ('competences', models.TextField(verbose_name='Compétences requises')),
                ('contact', models.EmailField(verbose_name='Pour postuler', default='admin@emploi.fr', max_length=254)),
                ('auteur', models.ForeignKey(verbose_name="Auteur de l'offre", to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
