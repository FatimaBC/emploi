from django import forms
from emploi.models import Offre

class OffreCreateForm(forms.ModelForm):
    class Meta:
        model = Offre
        fields = ('__all__')

class OffreUpdateForm(forms.ModelForm):
    class Meta:
        model = Offre
        fields = ('__all__')

class OffreDeleteForm(forms.ModelForm):
    class Meta:
        model=Offre
        fields = ('__all__')