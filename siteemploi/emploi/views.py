from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic import RedirectView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import CreateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from emploi.models import Offre
from django.contrib.auth import authenticate, login, logout as auth_logout
from django.http import *
from django.conf import settings
from emploi.forms import OffreCreateForm
from emploi.forms import OffreDeleteForm
from emploi.forms import OffreUpdateForm

# Create your views here.

class IndexView(TemplateView):
    """Page d'accueil"""
    template_name = 'emploi/index.html'
class LoginView(TemplateView):
    """Connexion."""
    template_name = 'emploi/login.html'
    def post(self, request, **kwargs):
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        user= authenticate(username= username, password= password)
        if user is not None and user.is_active:
            login(request,user)
            return HttpResponseRedirect( settings.LOGIN_REDIRECT_URL )
        return render(request, self.template_name)
class LogoutView(RedirectView):
    """Inscription"""
    url = "/"
    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return super(LogoutView,self).get(request, *args, **kwargs)
class OffresListView(ListView):
    """Liste des offres"""
    model = Offre
    context_object_name = "offres"
    template_name = "emploi/offres-list.html"
    paginate_by = None
class DepotOffreView(CreateView):
    """Creer une offre"""
    model = Offre
    form_class = OffreCreateForm
    template_name = 'emploi/offre-create.html'
    success_url = '/'
class DetailOffreView(DetailView):
    """Detail d'une offre"""
    model = Offre
    context_object_name = "offre"
    template_name = 'emploi/offre-detail.html'
class MyOffresView(ListView):
    """ Liste des offres publiées par l'utilisateur courant"""
    model = Offre
    context_object_name = "offres"
    template_name = 'emploi/my-offres.html'
    def get_queryset(self):
        return self.model.objects.filter(auteur_id=self.request.user)
class UpdateOffreView(UpdateView):
    model=Offre
    form_class = OffreUpdateForm
    template_name='emploi/edit-offre.html'
    success_url = '/'
class DeleteOffreView(DeleteView):
    model=Offre
    form_class = OffreDeleteForm
    template_name='emploi/offre-delete.html'
    success_url = '/'